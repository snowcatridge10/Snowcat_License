<Owner> <Year>
## The Software
This software may be freely distributed, forked, and copied. The user may not use a different license when this software is forked. There is no limitation to what the end user can use this software for. It may be claimed legally and without trouble.

## Forks
The end user is not allowed to sell their fork of this software or make it proprietary software or any other license. When forked, the original writer of the code does not have to help with technical issues. The writer of this software also does not have help with technical issues if the end user is using the software with its default source. You must credit the original software writer in your fork.

## Allowed Entities
### "Individuals"

Any "individual" is an "allowed entity" by itself, as long as it is using the licensed product not on behalf of a "company" or "non-profit". If this happens to be the case, their respective clauses shall be consulted instead.

### "Companies"

"Companies" are not "allowed entities" under this license.

### "Non-profits"

"Non-profits" are "allowed entities" under this license.

### Non-"allowed entity"

If you, as an entity are not an "allowed entity", you shall not use the product.

## Share-Alike
If you remix, transform, use, or build upon the material, you must distribute your work under the same license as this work.

## Money
Everyone, who IS NOT belonging to a company, industry or corporation,
can make money from the thing licensed under Snowcat License, but
must include link to the original source.

## No additional restrictions
You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits. 